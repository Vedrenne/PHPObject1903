<!doctype html>
<meta charset="UTF-8">


<?php
//Les inclusions

function autoloader_perso($className){
    var_dump($className);
    require('classes/' . $className . '.php');
    
}
spl_autoload_register('autoloader_perso');

//la fonction du dessus permet de charger les require et les include
//require('classes/Product.php');
//require('classes/cATEGORY.php');



//création d'un premier objet Product: un hamac
$hamac = new Product();
//changement des valeurs de propriétés du hamac
$hamac->name = 'Hamac2';
$hamac->description = 'Pour se reposer';
$hamac->price = '16.30';
//Debug du hamac
var_dump($hamac);

//création d'un premier objet Product: un parasol
$parasol = new Product();// avec la meme classe (Product), on peut créér deux objets différents.
//changement des valeurs de propriétés du parasol
$parasol->name = 'Parasol2';
$parasol->description = "Pour être à l'ombre";
$parasol->price = '30.00';
//Debug du parasol
var_dump($parasol);

$voyage = new Category();
var_dump($voyage);

//Affichage d'une propriété (price) du parasol
echo '<p>' . $parasol->price . '€</p>';

echo '<p>' . $parasol->displayPrice() . '</p>';
echo '<p>' . $hamac->displayPrice() . '</p>';

$voyage = new Category();

//Affichage d'une constante
echo '<p>Nombre de vues par défaut des produits : '. $hamac::DEFAULT_NB_VIEWS . '</p>';


?>
